package vaultkv

import (
	"context"
	"errors"
	"time"

	"github.com/go-kit/kit/endpoint"
)

type Endpoints struct {
	SetEndpoint endpoint.Endpoint
	GetEndpoint endpoint.Endpoint
}

func MakeServerEndpoints(s Service) Endpoints {
	return Endpoints{
		SetEndpoint: MakeSetEndpoint(s),
		GetEndpoint: MakeGetEndpoint(s),
	}
}

var (
	ErrInvalidRequest = errors.New("invalid request")
)

func MakeSetEndpoint(s Service) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (response interface{}, err error) {
		req := request.(setRequest)
		if len(req) != 1 {
			return setResponse{Err: ErrInvalidRequest}, nil
		}
		var item *Item
		for key, value := range req {
			item, err = s.Set(key, value)
		}
		if err != nil {
			return setResponse{Err: err}, nil
		}
		return setResponse{
			Key:       item.Key,
			Value:     item.Value,
			Timestamp: item.Timestamp.Unix(),
		}, nil
	}
}

func MakeGetEndpoint(s Service) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getRequest)
		item, err := s.Get(req.Key, req.Timestamp)
		if err != nil {
			return getResponse{Err: err}, nil
		}
		return getResponse{Value: item.Value}, nil
	}
}

type setRequest map[string]interface{}

type setResponse struct {
	Key       string      `json:"key,omitempty"`
	Value     interface{} `json:"value,omitempty"`
	Timestamp int64       `json:"timestamp,omitempty"`
	Err       error       `json:"err,omitempty"`
}

func (r setResponse) error() error { return r.Err }

type getRequest struct {
	Key       string
	Timestamp time.Time
}

type getResponse struct {
	Value interface{} `json:"value,omitempty"`
	Err   error       `json:"err,omitempty"`
}

func (r getResponse) error() error { return r.Err }
