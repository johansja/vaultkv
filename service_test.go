package vaultkv

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestInMemoryServiceSet(t *testing.T) {
	asserts := assert.New(t)
	svc := NewInMemoryService()

	t.Run("set once", func(t *testing.T) {
		item, err := svc.Set("key1", "value1")
		if asserts.NoError(err) {
			asserts.Equal("key1", item.Key)
			asserts.Equal("value1", item.Value)
			asserts.WithinDuration(time.Now(), item.Timestamp, time.Second)
		}
	})

	t.Run("set twice", func(t *testing.T) {
		item, err := svc.Set("key2", "value1")
		if asserts.NoError(err) {
			asserts.Equal("key2", item.Key)
			asserts.Equal("value1", item.Value)
			asserts.WithinDuration(time.Now(), item.Timestamp, time.Second)
		}

		item, err = svc.Set("key2", "value2")
		if asserts.NoError(err) {
			asserts.Equal("key2", item.Key)
			asserts.Equal("value2", item.Value)
			asserts.WithinDuration(time.Now(), item.Timestamp, time.Second)
		}
	})
}

func TestInMemoryServiceGet(t *testing.T) {
	asserts := assert.New(t)
	svc := &InMemoryService{
		kvStore: map[string][]*Item{
			"key1": {
				{
					Key:       "key1",
					Value:     "value1",
					Timestamp: time.Date(2019, time.January, 4, 23, 00, 0, 0, time.UTC),
				},
			},
			"key2": {
				{
					Key:       "key2",
					Value:     "value1",
					Timestamp: time.Date(2019, time.January, 4, 23, 43, 0, 0, time.UTC),
				},
				{
					Key:       "key2",
					Value:     "value2",
					Timestamp: time.Date(2019, time.January, 4, 23, 46, 0, 0, time.UTC),
				},
			},
		},
	}

	t.Run("key not found", func(t *testing.T) {
		item, err := svc.Get("key0", time.Now())
		if asserts.Error(err) {
			asserts.Nil(item)
		}
	})

	t.Run("value not found", func(t *testing.T) {
		item, err := svc.Get("key1", time.Date(2018, time.December, 31, 0, 0, 0, 0, time.UTC))
		if asserts.Error(err) {
			asserts.Nil(item)
		}
	})

	t.Run("only 1 version", func(t *testing.T) {
		item, err := svc.Get("key1", time.Date(2019, time.February, 1, 0, 0, 0, 0, time.UTC))
		if asserts.NoError(err) && asserts.NotNil(item) {
			asserts.Equal("key1", item.Key)
			asserts.Equal("value1", item.Value)
			asserts.Equal(time.Date(2019, time.January, 4, 23, 00, 0, 0, time.UTC), item.Timestamp)
		}
	})

	t.Run("latest version", func(t *testing.T) {
		item, err := svc.Get("key2", time.Date(2019, time.February, 1, 0, 0, 0, 0, time.UTC))
		if asserts.NoError(err) && asserts.NotNil(item) {
			asserts.Equal("key2", item.Key)
			asserts.Equal("value2", item.Value)
			asserts.Equal(time.Date(2019, time.January, 4, 23, 46, 0, 0, time.UTC), item.Timestamp)
		}
	})

	t.Run("earlier version", func(t *testing.T) {
		item, err := svc.Get("key2", time.Date(2019, time.January, 4, 23, 45, 0, 0, time.UTC))
		if asserts.NoError(err) && asserts.NotNil(item) {
			asserts.Equal("key2", item.Key)
			asserts.Equal("value1", item.Value)
			asserts.Equal(time.Date(2019, time.January, 4, 23, 43, 0, 0, time.UTC), item.Timestamp)
		}
	})
}
