module gitlab.com/johansja/vaultkv

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-kit/kit v0.8.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
)
