# Vault Dragon Key Value Store
This is done in respond to [this test](https://gist.github.com/jerelim/3e883999e8d8ef5af2428b364858afc3).

## Design
This is written in [Golang](https://golang.org/) and heavily relies on [Go kit](https://gokit.io/). Currently, the data is also stored in memory but it can be swapped out to database by swapping out the service implementation.

## Deployment
This is currently being deployed to Google App Engine at this [URL](https://js-vaultkv.appspot.com).

## Sample queries

[HTTPie](https://httpie.org/) is used for these

### 1 - Set key
```
> http https://js-vaultkv.appspot.com/object key1=value1

HTTP/1.1 200 OK
Alt-Svc: quic=":443"; ma=2592000; v="44,43,39,35"
Cache-Control: private
Content-Encoding: gzip
Content-Type: application/json; charset=utf-8
Date: Fri, 04 Jan 2019 17:54:38 GMT
Server: Google Frontend
Transfer-Encoding: chunked
Vary: Accept-Encoding
X-Cloud-Trace-Context: 00233d642aa10e6f8b22dcbc6985673a;o=1
```
```json
{
    "key": "key1",
    "timestamp": 1546624478,
    "value": "value1"
}
```

### 2 - Get key
```
> http https://js-vaultkv.appspot.com/object/key1

HTTP/1.1 200 OK
Alt-Svc: quic=":443"; ma=2592000; v="44,43,39,35"
Content-Length: 19
Content-Type: application/json; charset=utf-8
Date: Fri, 04 Jan 2019 17:54:47 GMT
Server: Google Frontend
X-Cloud-Trace-Context: a690bd5483312546a409eb5bce6643ed
```
```json
{
    "value": "value1"
}
```

### 3 - Get even earlier key
```
> http https://js-vaultkv.appspot.com/object/key1 timestamp==1234

HTTP/1.1 404 Not Found
Alt-Svc: quic=":443"; ma=2592000; v="44,43,39,35"
Cache-Control: private
Content-Encoding: gzip
Content-Type: application/json; charset=utf-8
Date: Fri, 04 Jan 2019 17:55:03 GMT
Server: Google Frontend
Transfer-Encoding: chunked
Vary: Accept-Encoding
X-Cloud-Trace-Context: 2fed937bcb0a831ba726dc85dc4024d8
```
```json
{
    "error": "item not found"
}
```

### 4 - Override key
```
)> http https://js-vaultkv.appspot.com/object key1=value2

HTTP/1.1 200 OK
Alt-Svc: quic=":443"; ma=2592000; v="44,43,39,35"
Cache-Control: private
Content-Encoding: gzip
Content-Type: application/json; charset=utf-8
Date: Fri, 04 Jan 2019 17:56:41 GMT
Server: Google Frontend
Transfer-Encoding: chunked
Vary: Accept-Encoding
X-Cloud-Trace-Context: e9afed376d6622b9cf1b25499e7edbbf;o=1
```
```json
{
    "key": "key1",
    "timestamp": 1546624601,
    "value": "value2"
}
```

### 5 - Get earlier key
```
> http https://js-vaultkv.appspot.com/object/key1 timestamp==1546624600

HTTP/1.1 200 OK
Alt-Svc: quic=":443"; ma=2592000; v="44,43,39,35"
Content-Length: 19
Content-Type: application/json; charset=utf-8
Date: Fri, 04 Jan 2019 17:56:53 GMT
Server: Google Frontend
X-Cloud-Trace-Context: 48ff044a8a6cf3b5e2e6aad6eda5f961;o=1
```
```json
{
    "value": "value1"
}
```

### 6 - Get non-existing key
```
> http https://js-vaultkv.appspot.com/object/key0

HTTP/1.1 404 Not Found
Alt-Svc: quic=":443"; ma=2592000; v="44,43,39,35"
Cache-Control: private
Content-Encoding: gzip
Content-Type: application/json; charset=utf-8
Date: Fri, 04 Jan 2019 17:57:20 GMT
Server: Google Frontend
Transfer-Encoding: chunked
Vary: Accept-Encoding
X-Cloud-Trace-Context: 4866b19e0160bf750bacfbc6b27192a0;o=1
```
```json
{
    "error": "key not found"
}
```

## What can be improved further
By utilizing Go kit, we can then easily add more middlewares (logging, metrics, tracing) as we needed.