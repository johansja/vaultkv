package vaultkv

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/go-kit/kit/log"
	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

var (
	ErrBadRouting       = errors.New("inconsistent mapping between route and handler (programmer error)")
	ErrInvalidTimestamp = errors.New("invalid timestamp")
)

func MakeHTTPHandler(s Service, logger log.Logger) http.Handler {
	r := mux.NewRouter()
	e := MakeServerEndpoints(s)
	options := []httptransport.ServerOption{
		httptransport.ServerErrorLogger(logger),
		httptransport.ServerErrorEncoder(encodeError),
	}

	r.Methods("POST").Path("/object").Handler(httptransport.NewServer(
		e.SetEndpoint,
		decodeSetRequest,
		encodeResponse,
		options...,
	))
	r.Methods("GET").Path("/object/{key}").Handler(httptransport.NewServer(
		e.GetEndpoint,
		decodeGetRequest,
		encodeResponse,
		options...,
	))

	return r
}

func decodeSetRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	var req setRequest
	if e := json.NewDecoder(r.Body).Decode(&req); e != nil {
		return nil, e
	}
	return req, nil
}

func decodeGetRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	vars := mux.Vars(r)
	key, ok := vars["key"]
	if !ok {
		return nil, ErrBadRouting
	}
	err = r.ParseForm()
	if err != nil {
		return nil, ErrBadRouting
	}
	timestamp := time.Now()
	if timestampForm := r.Form.Get("timestamp"); timestampForm != "" {
		timestampInt, err := strconv.ParseInt(timestampForm, 10, 64)
		if err != nil {
			return nil, ErrInvalidTimestamp
		}
		timestamp = time.Unix(timestampInt, 0)
	}
	return getRequest{
		Key:       key,
		Timestamp: timestamp,
	}, nil
}

type errorer interface {
	error() error
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(codeFrom(err))
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

func codeFrom(err error) int {
	switch err {
	case ErrKeyNotFound, ErrItemNotFound:
		return http.StatusNotFound
	case ErrInvalidRequest, ErrInvalidTimestamp:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
