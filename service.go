package vaultkv

import (
	"errors"
	"sync"
	"time"
)

type Service interface {
	Set(key string, value interface{}) (*Item, error)
	Get(key string, timestamp time.Time) (*Item, error)
}

type Item struct {
	Key       string      `json:"key"`
	Value     interface{} `json:"value"`
	Timestamp time.Time   `json:"timestamp"`
}

var (
	ErrKeyNotFound  = errors.New("key not found")
	ErrItemNotFound = errors.New("item not found")
)

type InMemoryService struct {
	mtx     sync.RWMutex
	kvStore map[string][]*Item
}

var _ Service = &InMemoryService{}

func NewInMemoryService() *InMemoryService {
	return &InMemoryService{
		kvStore: map[string][]*Item{},
	}
}

func (s *InMemoryService) Set(key string, value interface{}) (*Item, error) {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	item := &Item{
		Key:       key,
		Value:     value,
		Timestamp: time.Now(),
	}

	currentValue := s.kvStore[key]
	newValue := append(currentValue, item)
	s.kvStore[key] = newValue

	return item, nil
}

func (s *InMemoryService) Get(key string, timestamp time.Time) (*Item, error) {
	s.mtx.RLock()
	defer s.mtx.RUnlock()

	items, ok := s.kvStore[key]
	if !ok {
		return nil, ErrKeyNotFound
	}

	for i := len(items); i > 0; i-- {
		item := items[i-1]
		if item.Timestamp.Before(timestamp) {
			return item, nil
		}
	}

	return nil, ErrItemNotFound
}
